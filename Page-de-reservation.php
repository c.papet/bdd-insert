<?php session_start();


if(!empty($_SESSION['pseudo']))
{
echo 'Vous êtes connecté en tant que ' . $_SESSION['pseudo'];
}
else {
  echo 'Vous êtes connecté en tant que visiteur';
}

if (isset($_POST['submit'])) {
   
   $date_debut = $_POST['date_debut'];
   $date_fin = $_POST['date_fin'];
   $resa_sup_petitdej = $_POST['resa_sup_petitdej'];
   $resa_nb_adulte = $_POST['resa_nb_adulte'];  
   $resa_nb_enfant = $_POST['resa_nb_enfant']; 

   
  $bdd = new PDO('mysql:host=localhost;dbname=grandhotel_m2105', 'root', 'root'); 

  $sql = "INSERT INTO reservation (date_debut, date_fin, resa_sup_petitdej, resa_nb_adulte, resa_nb_enfant) VALUES (?,?,?,?,?)" ;
  $result = $bdd->prepare($sql);
  $result->execute([$date_debut, $date_fin, $resa_sup_petitdej, $resa_nb_adulte, $resa_nb_enfant]);
}

?>





<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="assets/css/foundation.css"  >
  <link rel="stylesheet" type="text/css" href="assets/css/css-page-de-garde.css"  >
	<title>Page de reservation</title>
</head>
<body>
  <header>
    <div id="reservation" class="image"> </div>

    <nav class="top-bar topbar-responsive">
      <div class="top-bar-title">
        <span data-responsive-toggle="topbar-responsive" data-hide-for="medium">
        </span>
        <a class="topbar-responsive-logo" href="#"><strong>Le Grand Hotel</strong></a>
      </div>
      <div id="topbar-responsive" class="topbar-responsive-links">
        <div class="top-bar-right">
          <ul class="menu simple vertical medium-horizontal">
            <li><a href="Page-de-garde.php">Retour &agrave la page d'accueil</a></li>
            <li><a href="#">Informations l&eacutegales</a></li>
            <li><a href="logout.php"> Se déconnecter
            <li><a href="Contact.html">Contact</a></li>
            <li>
              <button type="button" class="button hollow topbar-responsive-button">&Agrave propos</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>
   </header>

<div class="translucent-form-overlay">
  <form method="POST" action="Page-de-reservation.php">
    <h3>Formulaire de r&eacuteservation</h3>
    <div class="row columns">

          <div class="row"> 
      <label class="columns small-12">Periode de r&eacuteservation</label>
      <div class="columns small-6">
        <input type="date" min="0" name="date_debut" placeholder="JJ-MM-AAAA      Debut">
      </div>
      <div class="columns small-6">
        <input type="date" min="0" name="date_fin" placeholder="JJ-MM-AAAA      Fin">
      </div>
    </div>
      <label>Suplément petit déjeuner <br>
        <input type="checkbox" name="resa_sup_petitdej">
      </label>
      <label>Nommbre d'adultes
        <input type="text" name="resa_nb_adulte" placeholder="0">
      </label>
      <label>Nommbre d'enfants
        <input type="text" name="resa_nb_enfant" placeholder="0">
      </label>
 <!--     <label>Nombre de chambre
        <input type="text" name="keyword" placeholder="1">
      </label>
    </div>
    
    
    
    <div class="row columns">
      <label>Property Type
        <select name="status" type="text">
          <option>Any</option>
          <option value="office">Office</option>
          <option value="building">Building</option>
        </select>
      </label>
    </div>
    <div class="row columns">
      <label>Location
        <input type="text" name="location" placeholder="Any">
      </label>
    </div>
    -->
  <h3>Chambre</h3>
  <div class="row columns">
      <label>Etage
        <select name="etage" type="text">
          <option>Séléction</option>
          <option value="RDC">RDC</option>
          <option value="etage">Etage</option>
        </select>
      </label>
    </div>
   <label>Salle de bain<br>
      <input type="checkbox" name="bain">
    </label>
    <label>Wc<br>
      <input type="checkbox" name="wc">
    </label>
     <div class="row columns">
      <label>Nombre de lits
        <select name="nb_lits" type="text">
          <option>Séléction</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </label>
    </div>   
    
    
    <input type="submit" name="submit" value="Envoyer">
      
    
  
    <a href="log.php"><button type="button" class="primary button expanded search-button" >
      Se connecter/S'inscrire
    </button> </a>
 </form>
</div>
     
  
 


</body>
</html>
